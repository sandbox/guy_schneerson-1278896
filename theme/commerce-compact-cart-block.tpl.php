<?php
/**
 * @file
 * Default implementation of the shopping compact cart block template.
 *
 * Available variables:
 * - $order: The full order object for the shopping cart.
 * - $quantity : the number of products in the cart
 * - $currency_code : the currency of the current order
 * - $order_total : the totasl of the order (unformatted)
 * - $cart_total : this contains the total + currency (formatted)
 *
 * - cart_name : The text for the cart link display
 * - checkout_name : The text for the checkout link display
 * - cart_link : A full link or url to the cart
 * - checkout_link :  : A full link or url to the checkout
 */
  // Format some translatable strings for the cart.
  $cart_quantity_msg = t('Your Shopping Bag contains %quantity item(s)', array('%quantity' => $quantity));
  $cart_total_msg = t('Total %cart_total', array('%cart_total' => $cart_total));
?>
<div id="CompactCart">
  <div class="innerCompactCartContainer">
    <span class="CompactCart_qty_msg"><?php print $cart_quantity_msg ?> - <?php print $cart_total_msg; ?>
    <?php if ($cart_link): ?>
    <span class="CompactCart_lnk_cart"><?php print $cart_link; ?></span>
    <?php endif; ?>
    <?php if ($checkout_link): ?>
    <span class="CompactCart_lnk_chkout"><?php print $checkout_link; ?></span>
    <?php endif; ?>
  </div>
</div>
