<?php
/**
 * @file
 * Default implementation of the empty shopping compact cart block template.
 *
 * Add a span after the message to provide a link to your main catalogue page
 * e.g.
 * <span>
 * <a class="chkoutBtn" href="/products"><?php print t('Start Shopping'); ?></a>
 * </span>
 * Config allows you to use this or the main template.
 */
?>
<div id="CompactCart">
  <div class="innerCompactCartContainer">
  <span><?php print t('Your Shopping Bag is empty'); ?></span>
  </div>
</div>
