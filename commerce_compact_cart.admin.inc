<?php

/**
 * @file
 * Functions for configuring the compact cart.
 *
 * Variables set:
 *
 * compact_cart_visibility
 * compact_cart_pages
 * compact_cart_links
 * compact_cart_cartname
 * compact_cart_linkclass
 */

/**
 * Builds and returns the settings form.
 */
function commerce_compact_cart_admin_settings() {

  $form['description'] = array(
    '#type' => 'markup',
    '#markup' => t('Compact Cart Settings'),
  );

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Compact Cart settings'),
    '#weight' => 5,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['settings']['compact_cart_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show Compact on specific pages'),
    '#options' => array(0 => t('All pages except those listed'), 1 => t('Only the listed pages')),
    '#default_value' => variable_get('compact_cart_visibility', 0),
  );
  $form['settings']['compact_cart_pages'] = array(
    '#type' => 'textarea',
    '#title' => '<span class="element-invisible">' . t('Pages') . '</span>',
    '#default_value' => variable_get('compact_cart_pages', "cart\ncheckout/*"),
    '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %cart for the cart page and %checkout-wildcard for every checkout page. %front is the front page.",
            array(
              '%cart' => 'blog',
              '%checkout-wildcard' => 'checkout/*',
              '%front' => '<front>',
            )),
  );
  $form['settings']['compact_cart_emptytpl'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable template for empty cart'),
    '#default_value' => variable_get('compact_cart_emptytpl', 1),
    '#description' => t('This enables the custom template for the empty cart. If quantity is zero this template will be used.'),
  );

  $form['settings']['compact_cart_links'] = array(
    '#type' => 'radios',
    '#title' => t('How would you like the links generated?'),
    '#options' => array(0 => t('Full Links'), 1 => t('Only URLs')),
    '#default_value' => variable_get('compact_cart_links', 0),
    '#description' => t("Links to the cart/checkout can be full links (<a href='#' class='button inactive' disabled>%cart</a> - or - just the URLS for you to create your own links)",
             array(
               '%cart' => variable_get('compact_cart_cartname', 'Cart'),
             )),
  );

  $form['settings']['compact_cart_linkset'] = array(
    '#type' => 'radios',
    '#title' => t('Generated links/urls?'),
    '#options' => array(
      0 => t('Both'),
      1 => t('Cart Only'),
      2 => t('Checkout Only'),
    ),
    '#default_value' => variable_get('compact_cart_linkset', 0),
    '#description' => t("Would you like links to both the cart and the checkout"),
  );

  $form['settings']['compact_cart_cartname'] = array(
    '#type' => 'textfield',
    '#title' => t('Cart Name'),
    '#default_value' => variable_get('compact_cart_cartname', 'Cart') ,
    '#size' => 255,
    '#maxlength' => 255,
    '#description' => t("Text for the link to the Cart / Basket"),
    '#required' => TRUE,
  );
  $form['settings']['compact_cart_chkoutname'] = array(
    '#type' => 'textfield',
    '#title' => t('Checkout Name'),
    '#default_value' => variable_get('compact_cart_chkoutname', 'Checkout') ,
    '#size' => 255,
    '#maxlength' => 255,
    '#description' => t("Text for the link to the Checkout"),
    '#required' => TRUE,
  );
  $form['settings']['compact_cart_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Cart URL'),
    '#default_value' => variable_get('compact_cart_url', 'cart') ,
    '#size' => 255,
    '#maxlength' => 255,
    '#description' => t("The url to the cart"),
    '#required' => TRUE,
  );
  $form['settings']['compact_cart_linkclass'] = array(
    '#type' => 'textfield',
    '#title' => t('Link class'),
    '#default_value' => variable_get('compact_cart_linkclass', 'button') ,
    '#size' => 255,
    '#maxlength' => 255,
    '#description' => t("The class of links to cart / checkout"),
    '#required' => TRUE,
  );
  $form['settings']['compact_cart_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Compact Cart CSS'),
    '#default_value' => variable_get('compact_cart_css', drupal_get_path('module', 'commerce_compact_cart') . '/theme/commerce_compact_cart.css') ,
    '#size' => 255,
    '#maxlength' => 255,
    '#description' => t("The css file for the compact cart - leave blank for none"),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}
